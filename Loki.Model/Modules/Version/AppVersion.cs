﻿using System;

namespace Loki.Model.Modules.Version
{
    public class AppVersion : AbstractLokiModel<AppVersion>
    {
        public virtual string AppName { get; set; } 
        public virtual string Version { get; set; }
        public virtual DateTime Updated { get; set;}
    }
}
